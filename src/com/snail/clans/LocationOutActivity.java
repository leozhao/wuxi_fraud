package com.snail.clans;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.example.clans.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.snail.component.SImageButton;

/**
 * 任务大厅界面相关
 * 
 * @author zhaoys
 * 
 */
public class LocationOutActivity extends BaseActivity{
	
	@ViewInject(R.id.btnimg_phone)
	private SImageButton loginpwd;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location_out);
		ViewUtils.inject(this); // 注入view和事件
		
		loginpwd.setOnClickListener(new ClickListener());  
		
	}
	 @OnClick(R.id.btnimg_phone)
		public void btnimgPhoneOnclick(View v) {
		  	Toast.makeText(LocationOutActivity.this, "btnimgPhoneOnclick", Toast.LENGTH_SHORT).show();
		}
	 @OnClick(R.id.btnimg_card)
		public void btnimgCardOnclick(View v) {
			Toast.makeText(LocationOutActivity.this, "btnimgCardOnclick", Toast.LENGTH_SHORT).show();
		}
	
	 
	 public class ClickListener implements OnClickListener {  
	        @Override  
	        public void onClick(View v) {  
	            Log.e("test","onClick");  
	        }  
	    }  

}
