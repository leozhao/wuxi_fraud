package com.snail.clans;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.Toast;

import com.example.clans.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.snail.bean.Card;
import com.snail.component.STextFiled;

/**
 * 登录界面相关
 * 
 * @author zhaoys
 * 
 */
public class LoginActivity extends BaseActivity {

	public boolean loginErrorFlag = false;
	
	
	@ViewInject(R.id.txt_username)
	private STextFiled loginuser;
	
	@ViewInject(R.id.txt_password)
	private STextFiled loginpwd;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		ViewUtils.inject(this); //注入view和事件
		
		if (databaseCard != null && databaseCard.getUserName() != null)
			loginuser.setText(databaseCard.getUserName());
		
		loginpwd.editText.setOnKeyListener(onKey);
		/**
		 * 用一个定时器控制当打开这个Activity的时候就出现软键盘
		 */
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				showkeyBord();
			}
		}, 500);
	}

    @OnClick(R.id.btn_login)
	public void loginOnclick(View v) {
		loginRequest();
	}

	public void loginRequest() {

		if (TextUtils.isEmpty(loginuser.getText())) {
			Toast.makeText(LoginActivity.this, "用户名不能为空", Toast.LENGTH_LONG).show();
		} else if (TextUtils.isEmpty(loginpwd.getText())) {
			Toast.makeText(LoginActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
		} else {
			
			 //清空本地用户数据
			dbmCardManager.deleteCardAll(); 
	        
		databaseCard= new Card();
		databaseCard.setUserName("admin");
		databaseCard.setUserPwd("123456");
		databaseCard.setSubofficeId("230104");
		databaseCard.setWarehouseId("1");
		databaseCard.setLocalId("3000");
			dbmCardManager.add(databaseCard);
			changeActivity(LoginActivity.this,LocationOutActivity.class);
		}
	}

	public void login_regeditClick(View v) {
		//changeActivity(LoginActivity.this, RegeditActivity.class);
	}

	/**
	 * 忘记密码处理事件
	 * 
	 * @param v
	 */
	public void forgetPwdClick(View v) {
		//调用系统中浏览器访问该网址
		Uri uri = Uri.parse("http://security.woniu.com/retrieve");
		Intent it = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(it);
	}
	
	// ////////////////监听事件----------------
		OnKeyListener onKey = new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER
						&& event.getAction() == KeyEvent.ACTION_DOWN) {
					loginRequest();
					return true;
				}
				return false;
			}
		};
}
