package com.snail.clans;

import java.io.File;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.widget.Toast;

import com.example.clans.R;
import com.snail.utils.DatabaseCardUtil;
import com.snail.utils.ValidationUtil;

/**
 * 程序打开执行界面的类
 * 
 * @author zhaoys
 * 
 */
public class MainActivity extends BaseActivity {
	// 判断是否首次安装
	SharedPreferences share;
	Editor editor;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load);
		
		share = getSharedPreferences("count", MODE_WORLD_READABLE);
		// 操作sqlite数据库
		dbmCardManager = new DatabaseCardUtil(MainActivity.this);
		databaseCard = dbmCardManager.findFirstCard();
		
		if (ValidationUtil.isNetWork(this)) {
			int count = share.getInt("count", 0);
			
			// 判断是否首次登录程序 为0时为未激活
			if (count == 0) {
				// 蜗牛sdk 激活功能

				editor = share.edit();
				// 存入数据
				editor.putInt("count", 1);
				// 提交修改
				editor.commit();
			}

			// 创建缓存目录，系统一运行就得创建缓存目录的，
			cacheFile = new File(Environment.getExternalStorageDirectory(),"snailclanscache");
			if (!cacheFile.exists()) {
				cacheFile.mkdirs();
			}
			new Handler().postDelayed(new Runnable() {
				public void run() {
					if (databaseCard != null) {
						autoLogin();
					} else {
						changeActivity(MainActivity.this, LoginActivity.class);
					}
				}
			}, 2000); // 等待2秒进主界面
		} else {
		 	Toast.makeText(MainActivity.this, "无法连接网络", Toast.LENGTH_LONG).show();
			changeActivity(MainActivity.this, LoginActivity.class);
		}
	}

	/**
	 * 如果有用户信息app自动登录
	 */
	public void autoLogin() {
		changeActivity(MainActivity.this, LocationOutActivity.class);
	}
}
