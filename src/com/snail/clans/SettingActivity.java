package com.snail.clans;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.clans.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.snail.bean.Card;
import com.snail.component.STextFiledStr;

/**
 * 银行卡操作的类
 * 
 * @author zhaoys
 * 
 */
public class SettingActivity extends BaseActivity {

	@ViewInject(R.id.userPwd)
	private STextFiledStr txtUserPwd;

	@ViewInject(R.id.subofficeId)
	private STextFiledStr txtSubofficeId;

	@ViewInject(R.id.warehouseId)
	private STextFiledStr txtWarehouseId;

	@ViewInject(R.id.localId)
	private STextFiledStr txtLocalId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		ViewUtils.inject(this); // 注入view和事件

		if (null != databaseCard) {
			txtUserPwd.setText(databaseCard.getUserPwd());
			txtSubofficeId.setText(databaseCard.getSubofficeId());
			txtWarehouseId.setText(databaseCard.getWarehouseId());
			txtLocalId.setText(databaseCard.getLocalId());
		}
	}

	/**
	 * 显示兔兔币显示
	 * 
	 * @param v
	 */
	@OnClick(R.id.card_info_add_button)
	public void addCardInfoSettingClick(View v) {
		dbmCardManager.deleteCardAll();
		databaseCard = new Card();
		databaseCard.setUserName(databaseCard.getUserName());
		databaseCard.setUserPwd(txtUserPwd.getText().toString());
		databaseCard.setSubofficeId(txtSubofficeId.getText().toString());
		databaseCard.setWarehouseId(txtWarehouseId.getText().toString());
		databaseCard.setLocalId(txtLocalId.getText().toString());
		dbmCardManager.add(databaseCard);
		Toast.makeText(SettingActivity.this,"修改成功", Toast.LENGTH_LONG).show();
	}

}
