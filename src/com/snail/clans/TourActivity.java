package com.snail.clans;

import android.os.Bundle;
import android.view.View;

import com.example.clans.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.snail.component.STextFiledStr;

/**
 * 任务大厅界面相关
 * 
 * @author zhaoys
 * 
 */
public class TourActivity extends BaseActivity{

	@ViewInject(R.id.text_location)
	private STextFiledStr textLocation;

	@ViewInject(R.id.text_goods)
	private STextFiledStr textGoods;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour);
		ViewUtils.inject(this); // 注入view和事件
	}
	


	@OnClick(R.id.btn_goods)
	public void goodsOnclick(View v) {
	}

	@OnClick(R.id.btn_location)
	public void locationOnclick(View v) {
	}

	@OnClick(R.id.btn_location_out_enter)
	public void enterOnclick(View v) {

	}
}
