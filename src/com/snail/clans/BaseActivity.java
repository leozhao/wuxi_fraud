package com.snail.clans;

import java.io.File;
import java.util.Stack;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.snail.bean.Card;
import com.snail.bean.Transfer;
import com.snail.utils.DatabaseCardUtil;

/**
 * 共用界面处理类
 */
public class BaseActivity extends Activity {
	// 退出时间记录
	private long mExitTime;

	/**
	 * 操作sqlite数据库
	 */
	public static DatabaseCardUtil dbmCardManager;
	/**
	 * 数据库中保存的用户信息 或者 当前使用的用户
	 */
	public static Card databaseCard;

	
	/**
	 * 缓存文件夹
	 */
	public static File cacheFile;

	/**
	 * 判断是否在从界面上点击触发
	 */
	private static boolean isPageClick = true;
	/**
	 * 记录所有打开 的Activity
	 */
	private static Stack activityStack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); // 无title
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN); // 全屏
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//固定程序为竖屏
		
		if (activityStack == null) {
			activityStack = new Stack();
		}
	}


	/**
	 * 判断字符串是否为空或者null
	 * 
	 * @param temp
	 * @return true 不为空
	 */
	public boolean isNull(String temp) {
		boolean flag = true;
		if (temp == null || temp.equals("")) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 跳转界面处理方法
	 * 
	 * @param currentActivity
	 *            当前界面
	 * @param targetClass
	 *            跳转Class
	 */
	public void changeActivity(Activity currentActivity,
			Class<?> targetClass) {
		// 创建一个intent对象
		Intent intent = new Intent();
		// 指定原本的class和要启动的class
		intent.setClass(currentActivity, targetClass);
		// 调用另外一个新的Activity
		startActivity(intent);
		// 关闭原本的Activity
		
		activityStack.add(currentActivity);
		
		overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right); 
	}

	/**
	 * 跳转界面处理方法
	 * 
	 * @param currentActivity
	 *            当前界面
	 * @param targetClass
	 *            跳转Class
	 */
	public void changeActivity(Activity currentActivity, Class<?> targetClass,
			Transfer transfer) {
		// 创建一个intent对象
		Intent intent = new Intent();
		// 指定原本的class和要启动的class
		intent.setClass(currentActivity, targetClass);

		/* 通过Bundle对象存储需要传递的数据 */
		Bundle bundle = new Bundle();
		/* 字符、字符串、布尔、字节数组、浮点数等等，都可以传 */
		bundle.putSerializable("transfer", transfer);

		/* 把bundle对象assign给Intent */
		intent.putExtras(bundle);

		// 调用另外一个新的Activity
		startActivity(intent);
		
		
		// 关闭原本的Activity
		activityStack.add(currentActivity);
		
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		// 这里处理逻辑代码，大家注意：该方法仅适用于2.0或更新版的sdk
		if (isPageClick) {
			if ((System.currentTimeMillis() - mExitTime) > 3000) {
				Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
				mExitTime = System.currentTimeMillis();
			} else {
				finish();
				while (true) {
		            Activity activity = currentActivity();
		            if (activity == null) {
		                break;
		            }
		            popActivity(activity);
		        }
			}
		} else {
			isPageClick = true;
			super.onBackPressed();
		}
	}

	/**
	 * 返回上一级
	 * 
	 * @param temp
	 */
	public void goBack(View view) {
		isPageClick = false;
		onBackPressed();
	}
	

	/**
	 * 返回登录界面
	 * @param view
	 */
	public void goBackLogin(View view) {
		final boolean flag =  databaseCard != null && isNull(databaseCard.getUserName()) ;
		new AlertDialog.Builder(this).setTitle("系统提示")
				.setMessage(flag?"是否退出当前登录用户？":"是否确定返回登录界面")
				.setPositiveButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(flag)
							dbmCardManager.deleteCardAll();
						changeActivity(BaseActivity.this,LoginActivity.class);
					}
				}).setNegativeButton("否", null).show();

	}

	/**
	 * 返回巡视界面
	 * 
	 * @param view
	 */
	public void tourClick(View view) {
		changeActivity(BaseActivity.this, TourActivity.class);
	}

	/**
	 * 返回出入库界面
	 * 
	 * @param view
	 */
	public void locationoutClick(View view) {
		changeActivity(BaseActivity.this, LocationOutActivity.class);
	}
	
	/**
	 * 返回设置界面
	 * 
	 * @param view
	 */
	public void settingClick(View view) {
		changeActivity(BaseActivity.this, SettingActivity.class);
	}
	
	/**
	 *  获得当前栈顶Activity
	 * @return
	 */
    public Activity currentActivity() {
    	  Activity activity = null;
    	if(activityStack.size() != 0)
    		activity = (Activity)activityStack.lastElement();
        return activity;
    }
    
    /**
     * 退出栈顶Activity
     * @param activity
     */
    public void popActivity(Activity activity) {
        if (activity != null) {
            activity.finish();
            activityStack.remove(activity);
            activity = null;
        }
    }
    
    /**
     * 手动调用虚拟键盘 
     */
    public void showkeyBord(){
    	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);  
    	imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);  
    }
}
