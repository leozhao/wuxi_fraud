package com.snail.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clans.R;

/**
 * 文本框
 * 
 * @author zhaoys
 * 
 */
public class STextFiledStr extends LinearLayout {

	Context context;
	TextView textEditView; // 文本框右侧标识
	public EditText editText; // 输入框

	private String text; // editText中的数值
	private String hint; // editText中悬浮文字
	private boolean isPassWord; // 是否显示为密码
	private boolean isTopLine; // 是否显示上边线
	private boolean isNumber; // 是否显示为数字

	public STextFiledStr(Context context) {
		super(context);
		this.context = context;
		control();
	}

	/**
	 * 带初始数据实例化
	 * 
	 * @param context
	 * @param 初始数据
	 */
	public STextFiledStr(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_textfiled);

		text = a.getString(R.styleable.custom_textfiled_text);
		hint = a.getString(R.styleable.custom_textfiled_hint);
		isPassWord = a.getBoolean(R.styleable.custom_textfiled_isPassWord,false);
		isTopLine = a.getBoolean(R.styleable.custom_textfiled_isTopLine, false);
		isNumber = a.getBoolean(R.styleable.custom_textfiled_isNumber, false);
		control();

		a.recycle();
	}

	/**
	 * 初始控制界面
	 */
	private void control() {
		initialise(); // 实例化内部view
		setViewsLayoutParm(); // 设置内部view的布局参数
	}

	/**
	 * 实例化内部View
	 */
	private void initialise() {
			editText = new EditText(context);
			editText.setBackgroundResource(R.drawable.no_border_style);
			editText.setSingleLine();
			editText.setHint(hint);
			if (isNumber)
				editText.setInputType(InputType.TYPE_CLASS_NUMBER);
			if (isPassWord)
				editText.setInputType(InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_PASSWORD);
		
		textEditView = new TextView(context);
		
		textEditView.setText(text);
	}

	/**
	 * 设置内部view的布局参数
	 */
	private void setViewsLayoutParm() {
		
			textEditView.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
		//	textEditView.setPadding(10, 10, 0, 0);
			addView(textEditView);
			
			editText.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT, 1));
			addView(editText);
			

		if (isTopLine)
			this.setBackgroundResource(R.drawable.border_no_top_style);
		else
			this.setBackgroundResource(R.drawable.border_style);
		
		this.setMinimumHeight(85);
	}


	// ////////////公用方法/////////////////
	public String getText() {
		String temp = "";
		temp = editText.getText().toString();
		return temp;
	}

	/**
	 * 给文本框赋值
	 * @param temp
	 */
	public void setText(String temp) {
			editText.setText(temp);
	}
	
	
	public String getHint() {
		return editText.getHint().toString();
	}

	/**
	 * 
	 * @param temp
	 */
	public void setHint(String temp) {
		editText.setHint(temp);
	}
}
