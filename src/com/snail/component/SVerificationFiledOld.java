package com.snail.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clans.R;
import com.lidroid.xutils.BitmapUtils;

/**
 * 带有验证码的文本框
 * @author zhaoys
 * 
 */
public class SVerificationFiledOld extends LinearLayout {

	Context context;
	TextView textView; // 文本标识
	EditText editText; // 输入框
	ImageView imageButton;
	
	Bitmap bitmap = null; //图片资源

	private String text; // editText中的数值
	private boolean isTopLine; // 是否显示上边线

	public SVerificationFiledOld(Context context) {
		super(context);
		this.context = context;
		control();
	}

	/**
	 * 带初始数据实例化
	 * 
	 * @param context
	 * @param 初始数据
	 */
	public SVerificationFiledOld(Context context, int num) {
		super(context);
		this.context = context;
		control();
	}

	public SVerificationFiledOld(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_textfiled);

		text = a.getString(R.styleable.custom_textfiled_text);
		isTopLine = a.getBoolean(R.styleable.custom_textfiled_isTopLine, false);
		control();
		a.recycle();
	}

	/**
	 * 初始控制界面
	 */
	private void control() {
		initialise(); // 实例化内部view
		setViewsLayoutParm(); // 设置内部view的布局参数
		setViewListener();
	}

	/**
	 * 实例化内部View
	 */
	private void initialise() {
		textView = new TextView(context);
		editText = new EditText(context);
		imageButton = new ImageView(context);
		textView.setText(text);
		editText.setSingleLine();
		editText.setBackgroundResource(R.drawable.no_border_style);
	}

	/**
	 * 设置内部view的布局参数
	 */
	private void setViewsLayoutParm() {
		textView.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		editText.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 1));
		imageButton.setLayoutParams(new LayoutParams(
				99,
				LinearLayout.LayoutParams.MATCH_PARENT));
		imageButton.setScaleType(ScaleType.FIT_XY);
		addView(textView);
		addView(editText);
		addView(imageButton);

		if (isTopLine)
			this.setBackgroundResource(R.drawable.border_no_top_style);
		else
			this.setBackgroundResource(R.drawable.border_style);
	}

	/**
	 * 设置文本变化相关监听事件
	 */
	private void setViewListener() {
		imageButton.setOnClickListener(new OnButtonClickListener());
		editText.addTextChangedListener(new OnTextChangeListener());
	}

	public void setImageViewUrl() {
		BitmapUtils bitmapUtils = new BitmapUtils(context);
		// 加载网络图片
		bitmapUtils.display(imageButton, "http://dt-clan.woniu.com/dt/app/user/code");
	}
	
	/**
	 * 加减按钮事件监听器
	 * 
	 * @author ZJJ
	 * 
	 */
	class OnButtonClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			setImageViewUrl();
		}
	}

	/**
	 * EditText输入变化事件监听器
	 * 
	 * @author ZJJ
	 * 
	 */
	class OnTextChangeListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub

		}
	}
	
		//////////////公用方法/////////////////
		public String getText(){
			return editText.getText().toString();
		}
	
	
}
