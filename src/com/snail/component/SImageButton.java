package com.snail.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clans.R;

/**
 * 文本框
 * 
 * @author zhaoys
 * 
 */
public class SImageButton extends LinearLayout {

	SImageButton parent;
	Context context;
	TextView textView; // 文本框右侧标识
	ImageView imageView;// 图片

	private String text; // 图片下方文字
	private String src; // 图片地址

	public SImageButton(Context context) {
		super(context);
		this.context = context;
		parent = this;
		control();
	}

	/**
	 * 带初始数据实例化
	 * 
	 * @param context
	 * @param 初始数据
	 */
	public SImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		parent = this;
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_textfiled);

		text = a.getString(R.styleable.custom_textfiled_text);
		src = a.getString(R.styleable.custom_textfiled_src);
		
		control();

		a.recycle();
	}

	/**
	 * 初始控制界面
	 */
	private void control() {
		initialise(); // 实例化内部view
		setViewsLayoutParm(); // 设置内部view的布局参数
		
		  this.setOnTouchListener(new TouchListener()); 
	}

	/**
	 * 实例化内部View
	 */
	private void initialise() {
		imageView = new ImageView(context);
		int resId = this.getResources().getIdentifier(src, "drawable",
				context.getPackageName());
		imageView.setImageResource(resId);
		
		textView = new TextView(context);
		textView.setText(text);
	}

	/**
	 * 设置内部view的布局参数
	 */
	private void setViewsLayoutParm() {

		imageView.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 1));
		addView(imageView);		
		textView.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		addView(textView);
		
		this.setOrientation(LinearLayout.VERTICAL);
		this.setPadding(5, 5, 5, 5);
		this.setGravity(Gravity.CENTER_HORIZONTAL);
		this.setBackgroundResource(R.drawable.border_style);
	}


	public class TouchListener implements OnTouchListener{  
        @Override  
        public boolean onTouch(View v, MotionEvent event) {  
        	if (event.getAction() == MotionEvent.ACTION_DOWN) {  
        		parent.setBackgroundResource(R.drawable.border_no_style);
 	       }else if (event.getAction() == MotionEvent.ACTION_UP){
 	    	  parent.setBackgroundResource(R.drawable.border_style);
 	       }
        	return true;
        }   
    }        
}
