package com.snail.component.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.clans.R;

/**
 * 任务详情ListView使用
 * @author zhaoys
 *
 */
public class LocationOutAdapter extends BaseAdapter {
	private List<Map<String, Object>>  list;
	private LayoutInflater inflater;
	
	public LocationOutAdapter(Context context, List<Map<String, Object>> list){
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Map<String, Object> getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Map<String, Object> map = this.getItem(position);
		ViewHolder viewHolder;
		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = inflater.inflate(R.layout.location_out_report_table_item, null);
			viewHolder.mTextLocationInfo = (TextView) convertView.findViewById(R.id.text_location_info);
			viewHolder.mTextType = (TextView) convertView.findViewById(R.id.text_type);
			viewHolder.mTextNumber = (TextView) convertView.findViewById(R.id.text_number);
			viewHolder.mSelected = (CheckBox) convertView.findViewById(R.id.agreen);
			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		viewHolder.mTextLocationInfo.setText((String)map.get("locationInfo"));
		viewHolder.mTextType.setText((String)map.get("type"));
		viewHolder.mTextNumber.setText((String)map.get("number"));
		return convertView;
	}
	
	public static class ViewHolder{
		public TextView mTextLocationInfo;
		public TextView mTextType;
		public TextView mTextNumber;
		public CheckBox mSelected;
	}

}
