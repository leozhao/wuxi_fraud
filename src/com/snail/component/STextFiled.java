package com.snail.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clans.R;

/**
 * 文本框
 * 
 * @author zhaoys
 * 
 */
public class STextFiled extends LinearLayout {

	Context context;
	ImageView imageView;
	TextView textEditView; // 文本框右侧标识
	public EditText editText; // 输入框
	Button clearButton; // 清除按钮

	private String text; // editText中的数值
	private String value; // editText中的数值
	private String hint; // editText中悬浮文字
	private boolean isPassWord; // 是否显示为密码
	private boolean isTopLine; // 是否显示上边线
	private boolean isNumber; // 是否显示为数字
	private boolean isFocusable;// 文本框是否接受点击事件,如果不接受点击会将文本框变成文本
	private boolean isRequired; // 是否显示为必填项
	private boolean isVisibleIco; //是否显示

	public STextFiled(Context context) {
		super(context);
		this.context = context;
		control();
	}

	/**
	 * 带初始数据实例化
	 * 
	 * @param context
	 * @param 初始数据
	 */
	public STextFiled(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_textfiled);

		text = a.getString(R.styleable.custom_textfiled_text);
		value =  a.getString(R.styleable.custom_textfiled_value);
		hint = a.getString(R.styleable.custom_textfiled_hint);
		isPassWord = a.getBoolean(R.styleable.custom_textfiled_isPassWord,false);
		isTopLine = a.getBoolean(R.styleable.custom_textfiled_isTopLine, false);
		isNumber = a.getBoolean(R.styleable.custom_textfiled_isNumber, false);
		isFocusable = a.getBoolean(R.styleable.custom_textfiled_isFocusable,true);

		isRequired = a.getBoolean(R.styleable.custom_textfiled_isRequired, true);
		isVisibleIco = a.getBoolean(R.styleable.custom_textfiled_isVisibleIco, true);

		control();

		a.recycle();
	}

	/**
	 * 初始控制界面
	 */
	private void control() {
		initialise(); // 实例化内部view
		setViewsLayoutParm(); // 设置内部view的布局参数
		setViewListener();
	}

	/**
	 * 实例化内部View
	 */
	private void initialise() {
		imageView = new ImageView(context);

		if (isFocusable) {
			editText = new EditText(context);
			editText.setBackgroundResource(R.drawable.no_border_style);
			editText.setSingleLine();
			editText.setHint(hint);
			editText.setFocusable(isFocusable);
			if (isNumber)
				editText.setInputType(InputType.TYPE_CLASS_NUMBER);
			if (isPassWord)
				editText.setInputType(InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_PASSWORD);
		} else {
			textEditView = new TextView(context);
		}

		if(isVisibleIco){
        		int resId = this.getResources().getIdentifier(text, "drawable",context.getPackageName());
        		imageView.setImageResource(resId);
		}
		
		editText.setText(value);
		clearButton = new Button(context);

		clearButton.setText("显示");
		// 文字颜色
		clearButton.setTextColor(Color.rgb(102, 102, 102));
		clearButton.setBackgroundResource(R.drawable.button_style_gray);
	}

	/**
	 * 设置内部view的布局参数
	 */
	private void setViewsLayoutParm() {
		imageView.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		
		if(isVisibleIco)
			addView(imageView);

		if (isFocusable) {
			editText.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.MATCH_PARENT, 1));
			addView(editText);
		} else {
			textEditView.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.FILL_PARENT));
			textEditView.setPadding(10, 10, 0, 0);
			addView(textEditView);
		}

		if (isPassWord) {
			addView(clearButton);
		}

		if (isTopLine)
			this.setBackgroundResource(R.drawable.border_no_top_style);
		else
			this.setBackgroundResource(R.drawable.border_style);
		
		this.setMinimumHeight(85);
	}

	/**
	 * 设置文本变化相关监听事件
	 */
	private void setViewListener() {
		if (isPassWord) {
			clearButton.setOnClickListener(new OnButtonClickListener());
		}
		if (isFocusable) {
			// editText.setOnClickListener(new OnClickListener() {
			// @Override
			// public void onClick(View v) {
			// }
			// });
		}
	}

	/**
	 * 加减按钮事件监听器
	 * 
	 */
	class OnButtonClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (clearButton.getText().equals("显示")) {
				if (isFocusable)
					editText.setInputType(InputType.TYPE_CLASS_TEXT);
				clearButton.setText("隐藏");
			} else {
				if (isFocusable)
					editText.setInputType(InputType.TYPE_CLASS_TEXT
							| InputType.TYPE_TEXT_VARIATION_PASSWORD);
				clearButton.setText("显示");
			}
		}
	}

	// ////////////公用方法/////////////////
	public String getText() {
		String temp = "";
		if (isFocusable)
			temp = editText.getText().toString();
		else
			temp = textEditView.getText().toString();
		return temp;
	}

	/**
	 * 给文本框赋值
	 * @param temp
	 */
	public void setText(String temp) {
		if (isFocusable)
			editText.setText(temp);
		else {
			textEditView.setText(temp);
		}
	}
	
	
	public String getHint() {
		return editText.getHint().toString();
	}

	/**
	 * 
	 * @param temp
	 */
	public void setHint(String temp) {
		editText.setHint(temp);
	}
}
