package com.snail.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clans.R;

/**
 * 文本框
 * 
 * @author zhaoys
 * 
 */
public class ContextBox extends LinearLayout {

	LinearLayout parentLayout;

	Context context;
	// 头部容器
	LinearLayout topLayout;
	// 头部标题
	TextView topTitle;
	// 图片
	ImageView topIcon;

	/**
	 * 点击
	 */
	OnClickListener onButtonClickListener;

	/**
	 * 标题文字区域
	 */
	private String title;

	/**
	 * 标题icon地址
	 */
	private String icon;

	/**
	 * 是否展开
	 */
	private boolean isOpen;

	public ContextBox(Context context) {
		super(context);
		this.context = context;
		control();
	}

	/**
	 * 带初始数据实例化
	 * 
	 * @param context
	 * @param 初始数据
	 */
	public ContextBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		if (isInEditMode()) {// 不加入此处代码会导致可视化编辑报错
			return;
		}

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_textfiled);

		title = a.getString(R.styleable.custom_textfiled_text);
		icon = a.getString(R.styleable.custom_textfiled_icons);
		isOpen = a.getBoolean(R.styleable.custom_textfiled_isOpen, true);
		if (icon == null)
			icon = "arrow_up";
		control();

		a.recycle();
		
		if (!isOpen) {
			setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT, topLayout
							.getHeight()));
			icon = "arrow_down";
		}
		changeIcon();
	}

	
	void onWindowFocusChanged(){
		
	}
	
	
	/**
	 * 初始控制界面
	 */
	private void control() {
		initialise(); // 实例化内部view
		setViewsLayoutParm(); // 设置内部view的布局参数
		setViewListener();
	}

	/**
	 * 实例化内部View
	 */
	private void initialise() {

		parentLayout = this;

		topIcon = new ImageView(context);

		topTitle = new TextView(context);

		topTitle.setSingleLine();

		topTitle.setTextSize(16);

		topTitle.setText(title);

		topLayout = new LinearLayout(context);

		topLayout.setBackgroundResource(R.drawable.layout_border);
	}

	/**
	 * 设置内部view的布局参数
	 */
	private void setViewsLayoutParm() {
		setOrientation(VERTICAL);
		topLayout.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		addView(topLayout);

		topTitle.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 1));
		topTitle.setTextSize(18);
		topTitle.setPadding(10, 15, 10, 15);

		topLayout.addView(topTitle);

		topIcon.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT));

		topIcon.setPadding(10, 15, 10, 15);

		topLayout.addView(topIcon);
	}

	/**
	 * 设置文本变化相关监听事件
	 */
	private void setViewListener() {
		topIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeParentLayout();
			}
		});
	}

	// ////////////公用方法/////////////////
	public String getText() {
		return topTitle.getText().toString();
	}

	/**
	 * 给文本框赋值
	 * 
	 * @param temp
	 */
	public void setText(String temp) {
		topTitle.setText(temp);
	}

	/**
	 * 
	 */
	public void changeParentLayout() {
		// 展开或缩放到只剩余标题栏部分
		if (parentLayout.getHeight() == topLayout.getHeight()) {
			parentLayout.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			icon = "arrow_up";
		} else {
			parentLayout.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT, topLayout
							.getHeight()));
			icon = "arrow_down";
		}
		changeIcon();
	}

	/**
	 * 设置标题处图标
	 */
	public void changeIcon() {
		int resId = this.getResources().getIdentifier(icon, "drawable",
				context.getPackageName());
		topIcon.setImageResource(resId);
	}

}
