package com.snail.bean;

import org.json.JSONArray;

/**
 * 返回值共用类
 * @author zhaoys
 *
 */
public class Result {
	public Result() {
	}

	public Result(int code, String data, String message, String value) {
		this.setCode(code);
		this.setData(data);
		this.setMessage(message);
		this.setValue(value);
	}

	/**
     * 运行状态
     * 返回值：-1 执行出错
     *       0 执行正常
     */
	private int code = -1;
	/**
     * 
     */
	private String data;
	/**
     * 服务端信息提示
     */
	private String message;
	/**
     * 备用字段
     */
	private String value;
	
	/**
	 * 返回数据为数组时，值将在这里被使用
	 */
	public JSONArray dataArray;
	/**
	 * 分页使用
	 */
	private Page page;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

}
