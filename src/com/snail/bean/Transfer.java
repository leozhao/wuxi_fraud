package com.snail.bean;

import java.io.Serializable;

/**
 * Activity 之间传值对象
 * @author zhaoys
 *
 */
public class Transfer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Transfer() {
		// TODO Auto-generated constructor stub
	}


	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}


	/**
	 * 存储临时传递数据
	 */
	private Object data;
}
