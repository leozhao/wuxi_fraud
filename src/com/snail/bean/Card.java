package com.snail.bean;

import com.lidroid.xutils.db.annotation.Id;


/**
 * 用户银行卡保存信息实体
 * @author zhaoys
 *
 */
public class Card {


    /**
     * 持卡人姓名
     */
    @Id
    private String userName;  
    
    
    /**
     * 持卡人姓名
     */
    private String userPwd;  
    
    /**
     * 分局Id
     */
    private String subofficeId;  
    
    /**
     * 仓库Id
     */
    private String warehouseId;  
      
    /**
     * 本机Id
     */
    private String localId;  
    
    public Card() {  
    	
    }

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getSubofficeId() {
		return subofficeId;
	}

	public void setSubofficeId(String subofficeId) {
		this.subofficeId = subofficeId;
	}

	public String getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(String localId) {
		this.localId = localId;
	}


}
