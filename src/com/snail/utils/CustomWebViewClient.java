package com.snail.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * 网页加载前的提示框
 * @author zhaoys
 *
 */
public class CustomWebViewClient extends WebViewClient {

	
	public CustomWebViewClient(Context pcontext) {
		this.context = pcontext;
	}
	
	private Context context;
	
	private Dialog dialog;

	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {// 网页页面开始加载的时候
		dialog = DialogUtil.createLoadingDialog(context, "加载中。。。");
		dialog.show();
		view.setEnabled(false);// 当加载网页的时候将网页进行隐藏
		super.onPageStarted(view, url, favicon);
	}

	@Override
	public void onPageFinished(WebView view, String url) {// 网页加载结束的时候
		// super.onPageFinished(view, url);
		dialog.cancel();
		view.setEnabled(true);
	}

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) { // 网页加载时的连接的网址
		view.loadUrl(url);
		return false;
	}
}