package com.snail.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 数据验证类
 * 
 * @author zhaoys
 * 
 */
public class ValidationUtil {

	private static Pattern pattern;

	public ValidationUtil() {
	}

	/**
	 * 判断手机格式是否正确
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNO(String mobiles) {
		pattern = Pattern.compile("^((13[0-9])|(14[0-9])|(15[^4,\\D])|(17[0-9])|(18[0,5-9]))\\d{8}$");
		Matcher m = pattern.matcher(mobiles);

		return m.matches();
	}

	/**
	 * 判断email格式是否正确
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		pattern = Pattern.compile(str);
		Matcher m = pattern.matcher(email);
		return m.matches();
	}

	/**
	 * 判断是否全是数字
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		return isNum.matches();
	}

	/**
	 * 判断是否超出最大值
	 * 
	 * @param parameter
	 * @param len
	 * @return true 超出 false 未超出
	 */
	public static boolean isMaxLen(String parameter, int len) {
		boolean flag = false;
		if (parameter.length() > len)
			flag = true;
		return flag;
	}

	/**
	 * 根据传入字符判断时不再来 只含有汉字、数字、字母、下划线不能以下划线开头和结尾：
	 * 
	 * @param paramer
	 * @return
	 */
	public static boolean regUserName(String parameter) {
		String str = "^(?!_)(?!.*?_$)[a-zA-Z0-9]+$";
		pattern = Pattern.compile(str);
		Matcher isNum = pattern.matcher(parameter);
		return isNum.matches();
	}

	/**
	 * 判断是否有网络， true 有网络 false 无网络
	 * 
	 * @return
	 */
	public static boolean isNetWork(Context pcontext) {
		boolean cmflag = false;
		ConnectivityManager cm = (ConnectivityManager) pcontext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (null != cm) {
			// 如果仅仅是用来判断网络连接则可以使用 cm.getActiveNetworkInfo().isAvailable();
			NetworkInfo[] info = cm.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						cmflag = true;
					}
				}
			}
		}
		return cmflag;
	}
}
