package com.snail.utils;

import com.snail.bean.Result;


/**
 * 模拟传递方法名时使用
 * @author zhaoys
 *
 */
public interface CallBackInterface {

	/**
	 * 获取请求加载中执行情况调用
	 * @param total 总共大小
	 * @param current 当前已经请求的数量
	 * @param isUploading
	 */
	public void loading(long total, long current,
			boolean isUploading);
	
	/**
	 * 请求成功执行
	 * @param temp
	 */
	 public void success(Result temp);
	 /**
	  * 请求出错执行
	  * @param temp
	  */
	 public void failure(Result temp);

	 /**
	  * 开始调用请求时执行
	  */
	 public void start();
}
