package com.snail.utils;

import android.app.Dialog;
import android.content.Context;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.snail.bean.Result;

/**
 * Http地址请求工具类
 * 
 * @author zhaoys
 * 
 */
public class RequestUtil {
	
	/**
	 * 设置请求超时5秒钟
	 */
	private static final int REQUEST_TIMEOUT = 5 * 1000;

	/**
	 * Url共用部分网址
	 */
	private String urlAddress = "http://dt-clan.woniu.com/dt/app/";

	public RequestUtil() {

	}

	/**
	 * 
	 * @param purl
	 *            请求网址
	 * @param pparams
	 *            请求网址参数
	 * @param context
	 * @param ptitle
	 *            弹出加载框内容
	 * @param dialogFlag
	 *            弹出加载框 true 弹出
	 */
	public RequestUtil(String purl, RequestParams pparams, Context context,
			String ptitle, boolean dialogFlag) {
		this.url = urlAddress + purl;
		this.params = pparams;
		this.activity = context;
		this.title = (ptitle.equals("") ? "请稍候" : ptitle) + "...";
		isDialog(dialogFlag);
	}

	private Dialog dialog = null;

	/**
	 * 加载框内容
	 */
	public String title;

	/**
	 * 请求地址
	 */
	public String url;
	/**
	 * 请求参数
	 */
	public RequestParams params;
	/**
	 * 记录在哪个界面中运行
	 */
	public Context activity;

	/**
	 * 是否弹出加载框
	 * 
	 * @param dialogFlag
	 */
	public void isDialog(boolean dialogFlag) {
		if (dialogFlag) {
			dialog = DialogUtil.createLoadingDialog(activity, title);
			dialog.show();
		}
	}

	/**
	 * post请求
	 */
	public void sendPost() {
		if (null == mc)
			System.out.print("回调函数(CallBackInterface mc)不能为空");
		HttpUtils http = new HttpUtils();
		http.configTimeout(REQUEST_TIMEOUT);
		http.send(HttpRequest.HttpMethod.POST, url, params,
				new RequestCallBack<String>() {
					@Override
					public void onLoading(long total, long current,
							boolean isUploading) {
						mc.loading(total, current, isUploading);
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						Result tempResult = ResultUtil.getData(responseInfo.result);
						if(tempResult.getCode() != -1)
							mc.success(tempResult);
						else
							mc.failure(tempResult);
						
						if (null != dialog)
							dialog.cancel();
					}

					@Override
					public void onStart() {
						mc.start();
					}

					@Override
					public void onFailure(HttpException error, String msg) {
						Result tempResult = new Result();
						tempResult.setMessage("网络异常");
						mc.failure(tempResult);

						if (null != dialog)
							dialog.cancel();
					}
				});
	}

	/**
	 * post请求
	 */
	public void sendGet() {
		if (null == mc)
			System.out.print("回调函数(CallBackInterface mc)不能为空");

		HttpUtils http = new HttpUtils();
		http.configTimeout(REQUEST_TIMEOUT);
		http.send(HttpRequest.HttpMethod.GET, url,
				new RequestCallBack<String>() {
					@Override
					public void onLoading(long total, long current,boolean isUploading) {
						mc.loading(total, current, isUploading);
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
					    Result tempResult = ResultUtil.getData(responseInfo.result);
						if(tempResult.getCode() != -1)
							mc.success(tempResult);
						else
							mc.failure(tempResult);
						
						if (null != dialog)
							dialog.cancel();
					}

					@Override
					public void onStart() {
						mc.start();
					}

					@Override
					public void onFailure(HttpException error, String msg) {
						Result tempResult = new Result();
						tempResult.setMessage("网络异常");
						mc.failure(tempResult);
						if (null != dialog)
							dialog.cancel();
					}
				});
	}

	public CallBackInterface mc;

	public void setCallfuc(CallBackInterface mc) {
		this.mc = mc;
	}

}
