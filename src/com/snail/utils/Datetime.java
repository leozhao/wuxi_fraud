package com.snail.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.text.TextUtils;

/**
 * .<br>
 * <br>
 * CreateDate: 2013-7-8<br>
 * Copyright: Copyright(c) 2013-7-8<br>
 * <br>
 * 
 * @since v1.0.0
 * @Description 2013-7-8::::��������</br>
 */
public class Datetime {
	/**
	 * ��ָ����ʽ��ʾ��ǰ����ʱ��.<br>
	 * <br>
	 * 
	 * @param format
	 * @return
	 * @Description 2013-6-15::::�����˷���</br>
	 */
	public static String nowDateTime(String format) {
		if (TextUtils.isEmpty(format)) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		Calendar c = Calendar.getInstance();
		SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat(format);
		String now_date = simpleDateTimeFormat.format(c.getTime());
		return now_date;
	}

	/**
	 * ��ָ����ʽ��ʾ��ǰ����ʱ��.<br>
	 * <br>
	 * 
	 * @return
	 * @Description 2013-6-15::::�����˷���</br>
	 */
	public static String nowDateTime() {
		return nowDateTime("");
	}

	public static int compare(String data1, String data2) {
		int re = 0;

		java.text.DateFormat df = new java.text.SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		java.util.Calendar c1 = java.util.Calendar.getInstance();
		java.util.Calendar c2 = java.util.Calendar.getInstance();
		try {
			c1.setTime(df.parse(data1));
			c2.setTime(df.parse(data2));
		} catch (java.text.ParseException e) {
			System.err.println("��ʽ����ȷ");
		}
		re = c1.compareTo(c2);

		return re;
	}

	/**
	 * �õ�һ����ʽ����ʱ��
	 * 
	 * @param time
	 *            ʱ�� ����
	 * @return �֣��룺����
	 */
	public static String getFormatTime(long time) {
		long millisecond = time % 1000;
		long second = (time / 1000) % 60;
		long minute = time / 1000 / 60;

		// �����µ�ֻ��ʾһλ
		String strMillisecond = "" + (millisecond / 100);
		// ����ʾ��λ
		String strSecond = ("00" + second)
				.substring(("00" + second).length() - 2);
		// ����ʾ��λ
		String strMinute = ("00" + minute)
				.substring(("00" + minute).length() - 2);

		// return strMinute + ":" + strSecond + ":" + strMillisecond;
		return strMinute + ":" + strSecond;
	}
}
