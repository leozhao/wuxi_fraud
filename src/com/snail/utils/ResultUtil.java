package com.snail.utils;

import org.json.JSONException;
import org.json.JSONObject;

import com.snail.bean.Page;
import com.snail.bean.Result;

/**
 * 解析服务端返回值工具
 * 
 * @author zhaoys
 * 
 */
public class ResultUtil {
    /**
     * 通过字符串返回数据
     * 
     * @param parame
     * @return
     */
    public static Result getData(String parame) {
	Result s = new Result();
	JSONObject dataJson = null;
	try {
	    dataJson = new JSONObject(parame);
	    s.setCode(dataJson.getInt("code"));
	    if (isJsonArray(dataJson, "data")) {
		s.dataArray = dataJson.getJSONArray("data");
	    }
	    s.setData(dataJson.getString("data"));
	    s.setMessage(dataJson.getString("message"));
	    s.setValue(dataJson.getString("value"));
	    if (!dataJson.isNull("page")) {
		JSONObject pageJson = dataJson.getJSONObject("page");
		Page page = new Page();
		page.setCurrentPage(pageJson.getInt("currentPage"));
		page.setPageSize(pageJson.getInt("pageSize"));
		page.setTotalCount(pageJson.getInt("totalCount"));
		page.setTotalPage(pageJson.getInt("totalPage"));
		s.setPage(page);
	    }

	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return s;
    }

    /**
     * 判断parame在JsonObject是否为数组
     * 
     * @param parentObj
     * @param parame
     * @return
     */
    public static boolean isJsonArray(JSONObject parentObj, String parame) {
	boolean flag = true;
	try {
	    parentObj.getJSONArray("data");
	} catch (JSONException e1) {
	    flag = false;
	    e1.printStackTrace();
	}
	return flag;
    }

}
