package com.snail.utils;

import android.content.Context;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;
import com.snail.bean.Card;

/**
 * 操作sqlite 例： DbUtil db = new DbUtil(); db.create(MainActivity.this); //创建Card
 * Card Card = db.findCardById(1); //查找当前对象 Card.setEmail("shan@qq.com"); //更新对象
 * db.UpdateCard(Card); LogUtils.i(Cards.getEmail()); //输入Log日志
 * deleteCardById(Integer id) //删除对象
 * 
 * @author zhaoys
 * 
 */
public class DatabaseCardUtil {
    // xutils
    private DbUtils db;

    public DatabaseCardUtil(Context context) {
	db = DbUtils.create(context);
    }

    /**
     * 向数据库中添加数据
     * 
     * @param Card
     * @throws DbException
     */
    public void add(Card Card) {
	try {
	    db.save(Card);// 使用saveBindingId保存实体时会为实体的id赋值
	} catch (DbException e) {
	    LogUtils.i("添加用户出错");
	    e.printStackTrace();
	}
    }

    /**
     * 通过Id查找对应的Card
     * 
     * @param id
     *            唯一标识Id
     * @return
     * @throws DbException
     */
    public Card findCardById(Integer id) {
	Card entity = null;
	try {
	    entity = db.findById(Card.class, id); // db.findFirst(Selector.from(Parent.class).where("name","=","test"));
	} catch (DbException e) {
	    LogUtils.i("查找用户出错");
	    e.printStackTrace();
	}
	return entity;
    }

    /**
     * 查找Card
     * 
     * @return
     * @throws DbException
     */
    public Card findFirstCard() {
	Card entity = null;
	try {
	    entity = db.findFirst(Selector.from(Card.class).where("userName",
		    "!=", null));
	} catch (DbException e) {
	    LogUtils.i("查找用户出错");
	    e.printStackTrace();
	}
	return entity;
    }

    /**
     * 通过Id删除对应数据
     * 
     * @param id
     * @throws DbException
     */
    public void deleteCardById(String userName) {
	try {
	 db.deleteById(Card.class, userName);
	} catch (DbException e) {
	    LogUtils.i("删除用户出错");
	    e.printStackTrace();
	}
    }

    public void deleteCardAll() {
	try {
	    db.deleteAll(Card.class);
	    db.dropTable(Card.class); //清楚表结构
	} catch (DbException e) {
	    LogUtils.i("删除用户出错");
	    e.printStackTrace();
	}
    }

    /**
     * 更新用户信息
     * 
     * @param entity
     *            实体
     * @throws DbException
     */
    public void UpdateCard(Card entity) {
	deleteCardAll();
	add(entity);
    }

}
