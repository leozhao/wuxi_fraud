package com.example.bean;

import com.lidroid.xutils.db.annotation.Id;

/**
 * 用户保存信息实体
 * @author zhaoys
 *
 */
public class User {
    
    /**
     * 用户服务端Id
     */
	@Id
    private Integer userId;
    
    /**
     * 邮箱地址
     */
    private String email;  
    
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User() {  
    	
    }  
      
    public User(Integer userId,String email) {  
        this.userId = userId;
        this.email = email;
    }  
}
