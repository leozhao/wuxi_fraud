package com.example.utils;

import java.io.File;

import com.snail.util.HttpClientServer;

import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;

public class AsyncImageTask extends AsyncTask<String, Integer, Uri> {

		private ImageView iv_header;
		private File cache;

		public AsyncImageTask( ImageView iv_header ,File cache) {
			this.iv_header = iv_header;
			this.cache = cache;
		}

		// 后台运行的子线程子线�?
		@Override
		protected Uri doInBackground(String... params) {
			try {
				HttpClientServer s = new HttpClientServer();
				return s.getImageURI(params[0], cache);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		// 这个放在在ui线程中执�?
		@Override
		protected void onPostExecute(Uri result) {
			super.onPostExecute(result); 
			// 完成图片的绑�?
			if (iv_header != null && result != null) {
				iv_header.setImageURI(result);
			}
		}
	}