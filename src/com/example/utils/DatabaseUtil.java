package com.example.utils;

import android.content.Context;

import com.example.bean.User;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;

/**
 * 操作sqlite 
 *  例： 
 *  DbUtil db = new DbUtil();
 *  db.create(MainActivity.this);  //创建User
 *  User user = db.findUserById(1); //查找当前对象
 *  user.setEmail("shan@qq.com");  //更新对象
 *  db.UpdateUser(user);  
 *  LogUtils.i(users.getEmail());  //输入Log日志
 *  deleteUserById(Integer id) //删除对象
 * 
 * @author zhaoys
 * 
 */
public class DatabaseUtil {

	
	// xutils
	private DbUtils db;

	public void create(Context context) throws DbException {
		db = DbUtils.create(context);
		User user = new User(); // 这里需要注意的是User对象必须有id属性，或者有通过@ID注解的属性
		user.setEmail("wyouflf@qq.com");
		user.setUserId(1);
		db.save(user); // 使用saveBindingId保存实体时会为实体的id赋值
	}

	/**
	 * 通过Id查找对应的user
	 * 
	 * @param id
	 *            唯一标识Id
	 * @return
	 * @throws DbException
	 */
	public User findUserById(Integer id) throws DbException {
		User entity = db.findById(User.class, id); // or
													// db.findFirst(Selector.from(Parent.class).where("name","=","test"));
		return entity;
	}

	/**
	 * 通过Id删除对应数据
	 * 
	 * @param id
	 * @throws DbException
	 */
	public void deleteUserById(Integer id) throws DbException {
		db.deleteById(User.class, id);
	}

	/**
	 * 更新用户信息
	 * 
	 * @param entity
	 *            实体
	 * @throws DbException
	 */
	public void UpdateUser(User entity) throws DbException {
		db.update(entity);
	}

}
