package com.example.utils;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

import com.snail.util.HttpClientServer;
import com.snail.utils.CallBackInterface;

/**
 * 图片下载工作类
 * @author zhaoys
 *
 */
public class ImageUtil {
	
	public ImageUtil() {
	}

	public ImageUtil(ImageView imageView,Context context,String imageUrl,File cache) {
		this.imageView = imageView;
		this.activity = context;
		this.imageUrl = imageUrl;
		this.cache = cache;
	}

	private Bitmap imageBitmap = null; //图片资源
	public ImageView imageView;
	
	public String imageUrl;
	
	public File cache;
	
	
	private Thread thread;
	
	/**
	 * 记录在哪个界面中运行
	 */
	public Context activity;
	

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Bundle databundle = msg.getData();
			int code = databundle.getInt("code");
			if(null != imageBitmap)
				imageView.setImageBitmap(imageBitmap);
		}
	};
	

	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			 if(cache.canWrite()){
				 AsyncImageTask task = new AsyncImageTask(imageView,cache);
				 task.execute(imageUrl);
			 }else{
				 HttpClientServer https = new HttpClientServer();
				 imageBitmap =  https.returnBitMap(imageUrl);
			 }
			Message msg = new Message();
			Bundle data = new Bundle();
			data.putInt("code", 1);
			msg.setData(data);
			handler.sendMessage(msg);
		}
	};

	public void load() {
		thread = new Thread(runnable);
		thread.start();
	}

	public CallBackInterface mc;

	public void setCallfuc(CallBackInterface mc) {
		this.mc = mc;
	}

}
